﻿using UnityEngine;
using System.Collections;

public class WalkState2 : MonsterState
{
    public WalkState2(GameObject obj, MonsterStateManager state)
        : base(obj, state)
    {
        this._stateID = StateID.eStateID_Object_Walk;
    }
    public override void ProcessTransition()
    {
        if (m_Monster.MonsterData.curHp < 0)
        {
            m_StateManager.SetTransition(Transition.eTransiton_Object_Dead);
        }
        if (Player.MainPlayer != null && Vector3.Distance(Player.MainPlayer.transform.position, this.m_NavAgent.transform.position) < 1f)//m_NavAgent.velocity.magnitude < 0.1f
        {
            m_StateManager.SetTransition(Transition.eTransiton_Object_Attack);
        }
        if (PlayerData.Player_curHp <= 0)
        {
            m_StateManager.SetTransition(Transition.eTransiton_Object_Stand);
        }
    }
    public override void Update()
    {

    }

    public override void FixedUpdate()
    {

    }

    public override void OnEnter()
    {
		m_NavAgent.speed = 2.0f;
        m_Monster.MonsterAnimation("walk");
        m_AudioSource.clip = m_Monster.m_WalkAudioClip;
        m_AudioSource.loop = true;
        m_AudioSource.volume = 0.3f;
        m_AudioSource.Play();
        m_Model.transform.LookAt(Player.MainPlayer.transform.position);
    }

    public override void OnExit()
    {
        //m_NavAgent.Resume();
    }

    public void TimerEnd()
    {
    }
}
