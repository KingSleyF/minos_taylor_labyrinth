﻿using UnityEngine;
using System.Collections;

public class MonsterStateManager : MonoBehaviour
{

    private StateSystem m_StateSystem;
    void Awake()
    {
        InitStateSystem();
    }
    // Use this for initialization
    void Start()
    {
        Init();
    }
   
    // Update is called once per frame
    public void Update()
    {
        m_StateSystem.CurrentState.ProcessTransition();
    }

    public void FixedUpdate()
    {
        m_StateSystem.CurrentState.FixedUpdate();
    }
    public void InitStateSystem()
    {
        StandState2 stand = new StandState2(this.gameObject, this);
        stand.AddTransition(Transition.eTransiton_Object_Walk, StateID.eStateID_Object_Walk);
        stand.AddTransition(Transition.eTransiton_Object_Hurt, StateID.eStateID_Object_Hurt);
        stand.AddTransition(Transition.eTransiton_Object_Dead, StateID.eStateID_Object_Dead);
        stand.AddTransition(Transition.eTransiton_Object_Attack, StateID.eStateID_Object_Attack);
        stand.AddTransition(Transition.eTransiton_Object_Stand, StateID.eStateID_Object_Stand);

        BeatState2 attack = new BeatState2(this.gameObject, this);
        attack.AddTransition(Transition.eTransiton_Object_Walk, StateID.eStateID_Object_Walk);
        attack.AddTransition(Transition.eTransiton_Object_Stand, StateID.eStateID_Object_Stand);
        attack.AddTransition(Transition.eTransiton_Object_Hurt, StateID.eStateID_Object_Hurt);
        attack.AddTransition(Transition.eTransiton_Object_Dead, StateID.eStateID_Object_Dead);

        WalkState2 walk = new WalkState2(this.gameObject, this);
        walk.AddTransition(Transition.eTransiton_Object_Stand, StateID.eStateID_Object_Stand);
        walk.AddTransition(Transition.eTransiton_Object_Dead, StateID.eStateID_Object_Dead);
        walk.AddTransition(Transition.eTransiton_Object_Hurt, StateID.eStateID_Object_Hurt);
        walk.AddTransition(Transition.eTransiton_Object_Attack, StateID.eStateID_Object_Attack);

        HurtState2 hit = new HurtState2(this.gameObject, this);
        hit.AddTransition(Transition.eTransiton_Object_Stand, StateID.eStateID_Object_Stand);
        hit.AddTransition(Transition.eTransiton_Object_Dead, StateID.eStateID_Object_Dead);

        DeadState2 dead = new DeadState2(this.gameObject, this);

        m_StateSystem = new StateSystem();
        m_StateSystem.AddState(stand);
        m_StateSystem.AddState(attack);
        m_StateSystem.AddState(hit);
        m_StateSystem.AddState(dead);
        m_StateSystem.AddState(walk);
    }
    public void SetTransition(Transition t)
    {
        m_StateSystem.PerformTransition(t);
    }

    public StateID GetCurrentStateID()
    {
        return m_StateSystem.CurrentStateID;
    }

    public StateID GetLastStateID()
    {
        return m_StateSystem.LastStateID;
    }

    public State GetState(StateID id)
    {
        return m_StateSystem.GetState(id);
    }
	private void Init()
	{
		this.GetComponent<Animation>().Play("stand");
	}
}
