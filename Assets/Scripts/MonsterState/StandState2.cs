﻿using UnityEngine;
using System.Collections;

public class StandState2 : MonsterState
{

    public StandState2(GameObject obj, MonsterStateManager state)
        : base(obj, state)
    {
        _stateID = StateID.eStateID_Object_Stand;
    }

    public override void ProcessTransition()
    {
        if (m_Monster.MonsterData.curHp < 0)
        {
            m_StateManager.SetTransition(Transition.eTransiton_Object_Dead);
        }
		if (PlayerData.Player_curHp > 0 && m_NavAgent.velocity.magnitude > 0)
		{
			m_StateManager.SetTransition(Transition.eTransiton_Object_Walk);
		}
        if (PlayerData.Player_curHp <= 0)
        {
            m_StateManager.SetTransition(Transition.eTransiton_Object_Stand);
        }
    }
    public override void Update()
    {

    }

    public override void FixedUpdate()
    {

    }
    public override void OnEnter()
    {
        //Debug.Log("idlemonster");
		if (null != m_Animation)
		{
			m_Monster.MonsterAnimation("stand");
		}
		//m_NavAgent.Stop ();
    }

    public override void OnExit()
    {
        m_NavAgent.Resume();
    }
}
