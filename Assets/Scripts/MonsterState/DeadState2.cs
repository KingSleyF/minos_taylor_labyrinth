﻿using UnityEngine;
using System.Collections;

public class DeadState2 : MonsterState
{
    public DeadState2(GameObject obj, MonsterStateManager state)
        : base(obj, state)
    {
        _stateID = StateID.eStateID_Object_Dead;
    }

    public override void ProcessTransition()
    {

    }
    public override void Update()
    {

    }

    public override void FixedUpdate()
    {

    }

    public override void OnEnter()
    {
        //Debug.Log ("yaoguai die");
        m_Animation.Play("die");
        m_AudioSource.clip = m_Monster.m_DeadAudioClip;
        m_AudioSource.loop = false;
        m_AudioSource.volume = 1.0f;
        m_AudioSource.Play();
        m_NavAgent.Stop();
        m_Monster.StartCoroutine(AnimEnd(m_Animation["die"].length));
    }

    public override void OnExit()
    {

    }

    private IEnumerator AnimEnd(float time)
    {
        yield return new WaitForSeconds(time);
        m_NavAgent.enabled = false;
        Rigidbody rigiComponent = m_Model.GetComponent<Rigidbody>();
        if (rigiComponent != null)
        {
            rigiComponent.useGravity = true;
        }

        m_Monster.StartCoroutine(DestroyModel(0.5f));
    }

    private IEnumerator DestroyModel(float time)
    {
        yield return new WaitForSeconds(time);
        MonsterManager.Instance.DestroyMonster(m_Monster);
    }
}
