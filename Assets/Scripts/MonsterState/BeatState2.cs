﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BeatState2 : MonsterState
{
    bool m_bInState = false;
    public BeatState2(GameObject obj, MonsterStateManager state)
        : base(obj, state)
    {
        _stateID = StateID.eStateID_Object_Attack;
    }

    public override void ProcessTransition()
    {
        if (PlayerData.Player_curHp <= 0)
        {
            m_StateManager.SetTransition(Transition.eTransiton_Object_Stand);
        }
    }

    public override void Update()
    {

    }

    public override void FixedUpdate()
    {

    }

    public override void OnEnter()
    {
        //Debug.Log("attackmonster");
        m_bInState = true;
        if (null != m_Animation)
        {
            if(PlayerData.Player_curHp<=0)
            {
                m_StateManager.SetTransition(Transition.eTransiton_Object_Stand);
            }
            m_Monster.MonsterAnimation("injured");
            m_AudioSource.clip = m_Monster.m_BeatAudioClip;
            m_AudioSource.loop = false;
            m_AudioSource.volume = 1.0f;
            m_AudioSource.Play();
            m_Monster.StartCoroutine(AnimOver(m_Animation["injured"].length));
        }
    }

    public override void OnExit()
    {
        m_bInState = false;
    }

    IEnumerator AnimOver(float time)
    {
        yield return new WaitForSeconds(time);
        if (m_bInState)
        {
            Vector3 position = m_Model.transform.position + m_Model.transform.forward * 2.0f;
            UnityEngine.AI.NavMeshHit hit;
            if (UnityEngine.AI.NavMesh.SamplePosition(position, out hit, 500, 1))
            {
                m_NavAgent.SetDestination(hit.position);
            }
            // m_StateManager.SetTransition(Transition.eTransiton_Object_Walk);
            m_StateManager.SetTransition(Transition.eTransiton_Object_Stand);

            //m_AudioSource.clip = m_Player.m_AttackAudioClip02;
            //m_AudioSource.loop = false;
            //m_AudioSource.volume = 1.0f;
            //m_AudioSource.Play();
        }
    }
}
