﻿using UnityEngine;
using System.Collections;

public class HurtState2 : MonsterState
{
    bool m_bInState = false;

    public HurtState2(GameObject obj, MonsterStateManager state)
        : base(obj, state)
    {
        _stateID = StateID.eStateID_Object_Hurt;
    }

    public override void ProcessTransition()
    {
        if (m_Monster.MonsterData.curHp < 0)
        {
            m_StateManager.SetTransition(Transition.eTransiton_Object_Dead);
        }
    }
    public override void Update()
    {

    }

    public override void FixedUpdate()
    {

    }
    public override void OnEnter()
    {
        m_bInState = true;
        if (null != m_Animation)
        {
            Debug.Log("hurt");
            m_Animation.Play("hurt");
            m_AudioSource.clip = m_Monster.m_HitAudioClip;
            m_AudioSource.loop = false;
            m_AudioSource.volume = 1.0f;
            m_AudioSource.Play();
            m_Monster.StartCoroutine(AnimEnd(m_Animation["hurt"].length));
        }
    }

    public override void OnExit()
    {

    }

    IEnumerator AnimEnd(float time)
    {
        yield return new WaitForSeconds(time);
        if (m_bInState)
        {
            if (m_Monster.MonsterData.curHp < 0)
            {
                m_StateManager.SetTransition(Transition.eTransiton_Object_Dead);
            }
            m_StateManager.SetTransition(Transition.eTransiton_Object_Stand);
        }
    }
}
