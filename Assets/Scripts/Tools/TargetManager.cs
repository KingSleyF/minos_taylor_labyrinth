﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TargetManager  {

	public GameObject m_Target = null;

	public List<GameObject> m_TargetList = new List<GameObject>();

	static private TargetManager _instancec = null;
	static public TargetManager Instance
	{
		get{
			if (_instancec == null)
				_instancec = new TargetManager ();
			return _instancec;
			}
	}
}
