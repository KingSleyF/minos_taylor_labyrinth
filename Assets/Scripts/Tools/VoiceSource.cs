﻿using UnityEngine;
using System.Collections;

public class VoiceSource : MonoBehaviour
{
	private AudioSource sp;
    public AudioClip Hero_Attack;
	//public AudioClip Dragon_df_attack;
    public AudioClip Big_attack;
    public AudioClip be_attack;
    public AudioClip m_beatk;
    

       
    void Start()
    {
        
		sp = this.GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update() 
	{ 
   
    }
   
	public void Hero_attack()
    {
		sp.clip = Hero_Attack;
		sp.Play ();
    }
	/*public void dragon_df_attack()
    {
		sp.clip = Dragon_df_attack;
		sp.Play ();
    }*/
	public void big_attack()
    {
		sp.clip = Big_attack;
		sp.Play ();
    }
	public void Be_attack()
    {
		sp.clip = be_attack;
		sp.Play ();
    }
	public void m_be_attack()
	{
		sp.clip = m_beatk;
		sp.Play ();
	}
}
