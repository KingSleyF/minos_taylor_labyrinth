﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoMove : MonoBehaviour
{
    float timer;
    bool ismove;
    GameObject player;
    // Use this for initialization
    void Start()
    {
        player = GameObject.Find("Player");
        timer = 0;
        ismove = true;

    }

    // Update is called once per frame
    void Update()
    {
        if (ismove)
        {
            timer += Time.deltaTime;
            if (timer < 2)
            {
                //transform.LookAt(Vector3.left);
               player.transform.Translate(Vector3.forward * Time.deltaTime);
            }
            else if (timer < 4)
            {
                //transform.LookAt(Vector3.right);
                player.transform.Translate(Vector3.back * Time.deltaTime);
            }
            else
                ismove = false;
        }

    }
}
