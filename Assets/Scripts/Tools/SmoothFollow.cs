﻿using UnityEngine;
using System.Collections;

public class SmoothFollow : MonoBehaviour
{
    //跟随目标
    public Transform target;
    //摄像机距离
    public float distance = 4.0f;
    //摄像机高度
    public float height = 5.0f;
    //摄像机角度
    public float lookAtAngle = 0.0f;
    //虚拟目标物
    private GameObject targetGameObject;
    public void Start()
    {
        targetGameObject = new GameObject();
    }

   
    /// <summary>
    /// 摄像机基础跟随
    /// </summary>
    public void LateUpdate()
    {
        if (!target)
            return;
        //虚拟目标物与真实目标物 保持坐标和旋转角度一致
        targetGameObject.transform.rotation = target.rotation;
        targetGameObject.transform.position = target.position;
        //最初，摄像机与虚拟目标物保持位置相同
        this.transform.position = targetGameObject.transform.position;
        //设置高度 
        this.transform.position += Vector3.up * height;
        //设置距离
        this.transform.position -= distance * Vector3.forward;
        //设置旋转角度
        Quaternion currentRotation = Quaternion.Euler(0, lookAtAngle, 0);
        this.transform.position -= currentRotation * Vector3.forward;
        //LookAt
        this.transform.LookAt(targetGameObject.transform);
    }
    
	/*//*
 
    /// <summary>
    /// 摄像机跟随：判断边界
    /// </summary>
    public void LateUpdate()
    {
        if (!target)
            return;
        //虚拟目标物与真实目标物 保持坐标和旋转角度一致
        targetGameObject.transform.rotation = target.rotation;
        Vector3 realPos = target.position;
        if (realPos.x < -9.7f)
            realPos.x = -9.7f;
        if (realPos.x > 11.8f)
            realPos.x = 11.8f;
        if (realPos.z < -18.5f)
            realPos.z = -18.5f;
        if (realPos.z > 2.6f)
            realPos.z = 2.6f;
        //float edgesX = Mathf.Clamp(target.position.x, -9.7f, 11.8f);
        //float edgesZ = Mathf.Clamp(target.position.z, -18.5f, 2.6f);
        //Vector3 realPos = new Vector3(edgesX, target.rotation.y,edgesZ);
        targetGameObject.transform.position = realPos;
        //最初，摄像机与虚拟目标物保持位置相同
        this.transform.position = targetGameObject.transform.position;
        //设置高度 
        this.transform.position += Vector3.up * height;
        //设置距离
        this.transform.position -= distance * Vector3.forward;
        //设置旋转角度
        Quaternion currentRotation = Quaternion.Euler(0, lookAtAngle, 0);
        this.transform.position -= currentRotation * Vector3.forward;
        //LookAt
        this.transform.LookAt(targetGameObject.transform);
    }

    /*
    /// <summary>
    /// 摄像机跟随+判断边界+震动效果
    /// </summary>
    public void LateUpdate()
    {
        if (!target)
            return;
        //虚拟目标物与真实目标物 保持坐标和旋转角度一致
        targetGameObject.transform.rotation = target.rotation;
        Vector3 realPos = target.position;
        if (realPos.x < -9.7f)
            realPos.x = -9.7f;
        if (realPos.x > 11.8f)
            realPos.x = 11.8f;
        if (realPos.z < -18.5f)
            realPos.z = -18.5f;
        if (realPos.z > 2.6f)
            realPos.z = 2.6f;
        //float edgesX = Mathf.Clamp(target.position.x, -9.7f, 11.8f);
        //float edgesZ = Mathf.Clamp(target.position.z, -18.5f, 2.6f);
        //Vector3 realPos = new Vector3(edgesX, target.rotation.y,edgesZ);
        targetGameObject.transform.position = realPos;
        //最初，摄像机与虚拟目标物保持位置相同
        this.transform.position = targetGameObject.transform.position;
        //设置高度 
        this.transform.position += Vector3.up * height;
        //设置距离
        this.transform.position -= distance * Vector3.forward;
        //设置旋转角度
        Quaternion currentRotation = Quaternion.Euler(0, lookAtAngle, 0);
        this.transform.position -= currentRotation * Vector3.forward;
        //震动效果
        this.transform.position += hitShake;
        //LookAt
        if(lookAt)
            this.transform.LookAt(targetGameObject.transform);
    }
    private Vector3 hitShake = Vector3.zero;
    private bool lookAt = true;
    public void Hitcam(float factor)
    {
        lookAt = false;
        hitShake = (new Vector3(0.03f, 0.02f, 0.0f) * factor);
        StartCoroutine(Restcam());
    }

    private IEnumerator Restcam()
    {
        yield return new WaitForSeconds(0.05f);
        hitShake = Vector3.zero;
        lookAt = true;
    }

    void OnGUI()
    {
        if(GUILayout.Button("zhen dong"))
        {
            Hitcam(5f);
        }
    }
     */
}
