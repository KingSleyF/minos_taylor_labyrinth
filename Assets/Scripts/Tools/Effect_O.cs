﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Effect_O{
	private static Effect_O m_instance = null;
	public static Effect_O Instance
	{
		get
		{
			if (m_instance == null)
			{
				m_instance = new Effect_O();
			}
			return m_instance;
		}
	}



	Vector3 pos;
	GameObject  it = null;

	// Use this for initialization
	void Start () {
		
	
	}
	
	// Update is called once per frame
	void Update () {
		
	
	}
	public void BornEffect3()
	{
		pos = Player.MainPlayer.transform.position;
		pos = pos + Vector3.forward * 1.75f + Vector3.left * 0.9f + Vector3.down * 0.6f;

		Object prefab = Resources.Load ("Effect3");
		if (prefab != null) {
			GameObject monsterObj = GameObject.Instantiate (prefab) as GameObject;

			if (monsterObj != null) {
				monsterObj.transform.position = pos;

			}
			if (it == null) {
				
				it = monsterObj;
			} else {
				GameObject.Destroy (it);
				it = monsterObj;
			}

		}
	}

	
	public IEnumerator dealt(float time)
	{
		
		yield return new WaitForSeconds (time);

		GameObject.Destroy (it);
		it = null;
	}

}
