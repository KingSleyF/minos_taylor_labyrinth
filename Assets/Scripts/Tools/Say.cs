﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Say : MonoBehaviour
{//定义NPC对话数据  
    private string[] mData ={"你好,我是迷宫管理者","一个普普通通的NPC，不过我会告诉你：你需要做什么",
        "你现在正处在一个迷宫中,你需要想办法离开这个迷宫","迷宫里面有6个宝箱，找到他们后你就可以离开这里。"
        ,"现在就开是你的冒险吧"};
    //当前对话索引  
    private int index = 0;
    private int temp = 0;
    //用于显示对话的GUI Text  
    public Text mText;
    //对话标示贴图  
    //public Texture mTalkIcon;
    //是否显示对话标示贴图  
    private bool isTalk = false;
    private bool isFinish= false;
    bool isover;
    void Start()
    {
        mText.transform.parent.gameObject.SetActive(false);
    }

    void Update()
    {
        //从角色位置向NPC发射一条经过鼠标位置的射线  
        Ray mRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit mHi;

        //判断是否击中了NPC  
        if (Physics.Raycast(mRay, out mHi))//如果射线又遇到物体
        {
           // Debug.Log(mHi.collider.gameObject.tag);
            //如果击中了NPC  
            if (mHi.collider.gameObject.tag == this.gameObject.tag)
            { 
                if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
                {
                    //Debug.Log(mHi.collider.gameObject + mHi.collider.gameObject.tag + this.gameObject.tag);
                    if (baoxiang.baoxiangNUM == baoxiang.bxNUM)
                        isFinish = true;
                    else
                        isTalk = true;
                }
            }
        }
        if (isFinish)
        {
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
            {
                isover = true;
                mText.gameObject.SetActive(true);
                if (temp == 0)
                {
                    if (!mText.transform.parent.gameObject.activeSelf)
                        mText.transform.parent.gameObject.SetActive(true);
                    mText.text = "你找到了所有的宝箱！恭喜完成!";
                    temp++;
                }
                else
                {
                    mText.transform.parent.gameObject.SetActive(false);
                    temp = 0;
                    isFinish = false;
                    Debug.Log(" GAME　ＦＩＮＩＳＨ");
                    SceneManager.LoadScene("Begin");
                }
            }
        }
        else if (isTalk)
        {
            if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
            {
                isover = true;
                mText.gameObject.SetActive(true);
                if (index < mData.Length)
                {
                    if (!mText.transform.parent.gameObject.activeSelf)
                        mText.transform.parent.gameObject.SetActive(true);
                    mText.text = "迷宫管理者:" + mData[index];
                    index = index + 1;
                }
                else
                {
                    mText.transform.parent.gameObject.SetActive(false);
                    index = 0;
                    isTalk = false;
                    //mText.text = "NPC:" + mData[index];
                }
            }
            
        }
        


    }

   
  
}