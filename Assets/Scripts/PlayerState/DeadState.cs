﻿using UnityEngine;
using System.Collections;

public class DeadState : PlayerState
{
   
    public DeadState(GameObject obj, PlayerStateManager state)
        : base(obj, state)
    {
        this._stateID = StateID.eStateID_Object_Dead;
    }

    public override void ProcessTransition()
    {

    }

    public override void Update()
    {

    }

    public override void FixedUpdate()
    {

    }

    public override void OnEnter()
    {
        //Debug.Log("dead");
        m_Player.PlayerAnimation("die");
        //Vector3 position = m_Model.transform.position - m_Model.transform.forward * 2.0f;
        m_AudioSource.clip = m_Player.m_DieAudioClip;
        m_AudioSource.loop = false;
        m_AudioSource.volume = 1f;
        m_AudioSource.Play();
        //if(PlayerData.Plauer_curHp<=0)
        //{
        //    m_NavAgent.Stop();
        //}
    }

    public override void OnExit()
    {

    }

}
