﻿using UnityEngine;
using System.Collections;

public class HitState : PlayerState
{
    bool m_bInState = false;

    public HitState(GameObject obj, PlayerStateManager state)
        : base(obj, state)
    {
        _stateID = StateID.eStateID_Object_Hurt;
    }


    public override void ProcessTransition()
    {
        //   Debug.Log(m_NavAgent.velocity.magnitude);
        if (PlayerData.Player_curHp <= 0)
        {
            m_StateManager.SetTransition(Transition.eTransiton_Object_Dead);
        }
        else
        {
            m_StateManager.SetTransition(Transition.eTransiton_Object_Stand);
        }
    }
    public override void Update()
    {

    }

    public override void FixedUpdate()
    {

    }
    public override void OnEnter()
    {
        //Debug.Log("hit");
        m_bInState = true;
        if (null != m_Animation)
        {
            m_Animation.Play("hurt");
            PlayerData.Player_curHp -= 2;
            //Debug.Log("HP-2");
            m_AudioSource.clip = m_Player.m_HitAudioClip;
            m_AudioSource.loop = false;
            m_AudioSource.volume = 0.6f;
            m_AudioSource.Play();
            m_Player.StartCoroutine(AnimEnd(m_Animation["hurt"].length));
        }
        if (m_Player.m_Object != null)
        {
            Vector3 attackdir = m_Model.transform.position - m_Player.m_Object.transform.position;
            attackdir[1] = 0.0f;
            attackdir = Vector3.Normalize(attackdir);
            m_Model.GetComponent<Rigidbody>().AddForce(attackdir * 500.0f);
        }
    }

    public override void OnExit()
    {
        m_bInState = false;
    }

    IEnumerator AnimEnd(float time)
    {
        yield return new WaitForSeconds(time);
        if(m_bInState)
        {
            m_StateManager.SetTransition(Transition.eTransiton_Object_Stand);
        }
    }
}
