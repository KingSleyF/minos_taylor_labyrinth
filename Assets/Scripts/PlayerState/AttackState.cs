﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AttackState : PlayerState
{
    bool m_bInState = false;
    public AttackState(GameObject obj, PlayerStateManager state)
        : base(obj, state)
    {
        _stateID = StateID.eStateID_Object_Attack;
    }

    public override void ProcessTransition()
    {


    }

    public override void Update()
    {

    }

    public override void FixedUpdate()
    {

    }

    public override void OnEnter()
    {
        m_bInState = true;
        if (null != m_Animation)
        {
            m_Player.PlayerAnimation("injured");
            m_AudioSource.clip = m_Player.m_AttackAudioClip;
            m_AudioSource.loop = true;
            m_AudioSource.volume = 1f;
            m_AudioSource.Play();
            m_Player.StartCoroutine(AnimOver(m_Animation["injured"].length));

            m_Player.StartCoroutine(Attack(m_Animation["injured"].length / 2));
        }

        if (TargetManager.Instance.m_Target != null)
        {
            m_NavAgent.SetDestination(TargetManager.Instance.m_Target.transform.position);
        }
        //m_AudioSource.clip = m_Player.m_AttackAudioClip;
        //m_AudioSource.loop = false;
        //m_AudioSource.volume = 1.0f;
        //m_AudioSource.Play();
    }

    public override void OnExit()
    {
        m_bInState = false;
    }

    IEnumerator AnimOver(float time)
    {
        yield return new WaitForSeconds(time);
        if (m_bInState)
        {
            Vector3 position = m_Model.transform.position + m_Model.transform.forward * 1.0f;
            UnityEngine.AI.NavMeshHit hit;
            if (UnityEngine.AI.NavMesh.SamplePosition(position, out hit, 500, 1))
            {
                m_NavAgent.SetDestination(hit.position);
            }
            m_StateManager.SetTransition(Transition.eTransiton_Object_Walk);
        }
    }

    IEnumerator Attack(float time)
    {
        yield return new WaitForSeconds(time);
        int rands = (int)Random.Range(0, 1);
        if (m_bInState)
        {
            List<GameObject> monsters = TargetManager.Instance.m_TargetList;
            for (int i = 0; i < monsters.Count; i++)
            {
                GameObject obj = monsters[i];
                if (obj != null)
                {
                    Monster monsterScrip = obj.GetComponent<Monster>();
                    if (monsterScrip != null)
                    {
                        if(rands == 1)
                        {
                            monsterScrip.OnHurt(10);
                        }
                        else
                        {
                            monsterScrip.OnHurt(5);
                        }
                        MonsterStateManager monsterState = obj.GetComponent<MonsterStateManager>();
                        if (monsterState != null)
                        {
                            if (monsterState.GetCurrentStateID() != StateID.eStateID_Object_Dead)
                            {
                                if (monsterScrip.MonsterData.curHp <= 0)
                                {
                                    PlayerData.Beat_Count++;
                                    monsterState.SetTransition(Transition.eTransiton_Object_Dead);
                                }
                                else
                                {
                                    monsterState.SetTransition(Transition.eTransiton_Object_Hurt);
                                }
                            }
                        }
                    }
                }
            }

        }
    }
}
