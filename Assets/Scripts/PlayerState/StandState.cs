﻿using UnityEngine;
using System.Collections;

public class StandState : PlayerState
{

    public StandState(GameObject obj, PlayerStateManager state)
        : base(obj, state)
    {
        _stateID = StateID.eStateID_Object_Stand;
    }


    public override void ProcessTransition()
    {
     //   Debug.Log(m_NavAgent.velocity.magnitude);
        if (PlayerData.Player_curHp <= 0)
        {
            m_StateManager.SetTransition(Transition.eTransiton_Object_Dead);
        }
        if (m_NavAgent.velocity.magnitude > 0)
		{
			m_StateManager.SetTransition(Transition.eTransiton_Object_Walk);
        }
        if (0 != MonsterManager.Instance.GetMonsterCount())
        {
            m_StateManager.SetTransition(Transition.eTransiton_Object_Attack);
        }
    }
    public override void Update()
    {

    }

    public override void FixedUpdate()
    {

    }
    public override void OnEnter()
    {
        //Debug.Log("idle");
		if (null != m_Animation)
		{
			m_Player.PlayerAnimation("idle");
		}
		//m_NavAgent.Stop ();
    }

    public override void OnExit()
    {
        m_NavAgent.isStopped=false;
    }
}
