﻿using UnityEngine;
using System.Collections;

public class PlayerStateManager : MonoBehaviour
{
    private StateSystem m_StateSystem;
    void Awake()
    {
        InitStateSystem();
    }
    // Use this for initialization
    void Start()
    {
        Init();
    }
   
    // Update is called once per frame
    public void Update()
    {
        m_StateSystem.CurrentState.ProcessTransition();
    }

    public void FixedUpdate()
    {
        m_StateSystem.CurrentState.FixedUpdate();
    }
    public void InitStateSystem()
    {
        StandState stand = new StandState(this.gameObject, this);
        stand.AddTransition(Transition.eTransiton_Object_Attack, StateID.eStateID_Object_Attack);
        stand.AddTransition(Transition.eTransiton_Object_Run, StateID.eStateID_Object_Run);
        stand.AddTransition(Transition.eTransiton_Object_Walk, StateID.eStateID_Object_Walk);
        stand.AddTransition(Transition.eTransiton_Object_Dead, StateID.eStateID_Object_Dead);
        stand.AddTransition(Transition.eTransiton_Object_Hurt, StateID.eStateID_Object_Hurt);

        WalkState walk = new WalkState(this.gameObject, this);
        walk.AddTransition(Transition.eTransiton_Object_Attack, StateID.eStateID_Object_Attack);
        walk.AddTransition(Transition.eTransiton_Object_Run, StateID.eStateID_Object_Run);
        walk.AddTransition(Transition.eTransiton_Object_Dead, StateID.eStateID_Object_Dead);
        walk.AddTransition(Transition.eTransiton_Object_Hurt, StateID.eStateID_Object_Hurt);
        walk.AddTransition(Transition.eTransiton_Object_Stand, StateID.eStateID_Object_Stand);
        walk.AddTransition(Transition.eTransiton_Object_Walk, StateID.eStateID_Object_Walk);

        RunState run = new RunState(this.gameObject, this);
        run.AddTransition(Transition.eTransiton_Object_Attack, StateID.eStateID_Object_Attack);
        run.AddTransition(Transition.eTransiton_Object_Stand, StateID.eStateID_Object_Stand);
        run.AddTransition(Transition.eTransiton_Object_Walk, StateID.eStateID_Object_Walk);
        run.AddTransition(Transition.eTransiton_Object_Run, StateID.eStateID_Object_Run);

        AttackState attack = new AttackState(this.gameObject, this);
        attack.AddTransition(Transition.eTransiton_Object_Run, StateID.eStateID_Object_Run);
        attack.AddTransition(Transition.eTransiton_Object_Walk, StateID.eStateID_Object_Walk);
        attack.AddTransition(Transition.eTransiton_Object_Dead, StateID.eStateID_Object_Dead);
        attack.AddTransition(Transition.eTransiton_Object_Stand, StateID.eStateID_Object_Stand);

        HitState hurt = new HitState(this.gameObject, this);
        hurt.AddTransition(Transition.eTransiton_Object_Stand, StateID.eStateID_Object_Stand);
        hurt.AddTransition(Transition.eTransiton_Object_Walk, StateID.eStateID_Object_Walk);
        hurt.AddTransition(Transition.eTransiton_Object_Dead, StateID.eStateID_Object_Dead);

        DeadState dead = new DeadState(this.gameObject, this);

        m_StateSystem = new StateSystem();
        m_StateSystem.AddState(stand);
        m_StateSystem.AddState(run);
        m_StateSystem.AddState(walk);
        m_StateSystem.AddState(hurt);
        m_StateSystem.AddState(attack);
        m_StateSystem.AddState(dead);
    }
    public void SetTransition(Transition t)
    {
        m_StateSystem.PerformTransition(t);
    }

    public StateID GetCurrentStateID()
    {
        return m_StateSystem.CurrentStateID;
    }

    public StateID GetLastStateID()
    {
        return m_StateSystem.LastStateID;
    }

    public State GetState(StateID id)
    {
        return m_StateSystem.GetState(id);
    }
	private void Init()
	{
		this.GetComponent<Animation>().Play("idle");
	}
}
