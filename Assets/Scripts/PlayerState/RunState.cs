﻿using UnityEngine;
using System.Collections;

public class RunState : PlayerState
{
    bool m_bInState = false;
    public RunState(GameObject obj, PlayerStateManager state)
        : base(obj, state)
    {
        this._stateID = StateID.eStateID_Object_Run;
    }

    public override void ProcessTransition()
    {
        if (!m_Animation.isPlaying)
        {
            m_StateManager.SetTransition(Transition.eTransiton_Object_Walk);
        }
        //if (0 != MonsterManager.Instance.GetMonsterCount())
        //{
        //    m_StateManager.SetTransition(Transition.eTransiton_Object_Attack1);
        //}
    }
    public override void Update()
    {

    }

    public override void FixedUpdate()
    {

    }

    public override void OnEnter()
    {
        m_bInState = true;
       // m_NavAgent.Resume();
        m_Player.PlayerAnimation("run");
        m_Player.StartCoroutine(AudioBeg(m_Animation["run"].length*0.1f));
        m_Player.StartCoroutine(AnimOver(m_Animation["run"].length));
    }
    
    public override void OnExit()
    {
        m_NavAgent.speed = PlayerData.SpeedRun;
        m_bInState = false;
    }

    IEnumerator AudioBeg(float time)
    {
        yield return new WaitForSeconds(time);
        m_AudioSource.clip = m_Player.m_RunAudioClip;
        m_AudioSource.loop = false;
        m_AudioSource.volume = 0.6f;
        m_AudioSource.Play();
    }

    IEnumerator AnimOver(float time)
    {
        yield return new WaitForSeconds(time);
        if (m_bInState)
        {
            m_NavAgent.speed = PlayerData.SpeedRun;
            Vector3 position = m_Model.transform.position + m_Model.transform.forward * 12.0f;
            UnityEngine.AI.NavMeshHit hit;
            if (UnityEngine.AI.NavMesh.SamplePosition(position, out hit, 500, 1))
            {
                m_NavAgent.speed = PlayerData.SpeedRun;
                m_NavAgent.SetDestination(hit.position);
            }
            //if (0 != MonsterManager.Instance.GetMonsterCount())
            //{
            //    m_StateManager.SetTransition(Transition.eTransiton_Object_Attack);
            //}
            m_StateManager.SetTransition(Transition.eTransiton_Object_Walk);

        }
    }
    //IEnumerable AnimEnd(float time)
    //{
    //    yield return new WaitForSeconds(time);
    //    if (m_bInState)
    //    {
            
    //        Vector3 position = m_Model.transform.position + m_Model.transform.forward * 5.0f;
    //        NavMeshHit hit;
    //        if (NavMesh.SamplePosition(position, out hit, 500, 1))
    //        {
    //            m_NavAgent.SetDestination(hit.position);
    //        }
    //        m_StateManager.SetTransition(Transition.eTransiton_Object_Walk);
    //    }
    //}
}
