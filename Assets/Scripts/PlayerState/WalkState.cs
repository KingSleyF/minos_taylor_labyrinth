﻿using UnityEngine;
using System.Collections;

public class WalkState : PlayerState
{
    public WalkState(GameObject obj, PlayerStateManager state)
        : base(obj, state)
    {
        _stateID = StateID.eStateID_Object_Walk;
    }

    public override void ProcessTransition()
    {
        if (PlayerData.Player_curHp <= 0)
        {
            m_StateManager.SetTransition(Transition.eTransiton_Object_Dead);
        }
        if (m_NavAgent.velocity.magnitude <= 0.1f)
        {
            m_StateManager.SetTransition(Transition.eTransiton_Object_Stand);
        }
        if (0 != MonsterManager.Instance.GetMonsterCount())
        {
            m_StateManager.SetTransition(Transition.eTransiton_Object_Attack);
        }
    }

    public override void Update()
    {

    }

    public override void FixedUpdate()
    {

    }
    public override void OnEnter()
    {
        //Debug.Log("walk");
        m_Player.PlayerAnimation("walk");
        m_AudioSource.clip = m_Player.m_WalkAudioClip;
        m_AudioSource.loop = true;
        m_AudioSource.volume = 0.4f;
        m_AudioSource.Play();
        m_NavAgent.speed = PlayerData.SpeedWalk;
        m_Player.StartCoroutine(AnimOver(m_Animation["walk"].length));
    }
    public override void OnExit()
    {

    }
    IEnumerator AnimOver(float time)
    {
        yield return new WaitForSeconds(time);
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            LayerMask mask = 1 << 8;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask))
            {
                m_NavAgent.SetDestination(hit.point);
                m_StateManager.SetTransition(Transition.eTransiton_Object_Walk);
            }
        }
    }
}
