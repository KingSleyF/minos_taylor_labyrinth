﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class baoxiang : MonoBehaviour {

    // Use this for initialization
    static public int baoxiangNUM=0;
    //当前对话索引  
    static public int bxNUM =6;
    public Text mText;
    //对话标示贴图  
    //public Texture mTalkIcon;
    //是否显示对话标示贴图  
    private bool isFirst = true;
    private bool isFirst2 = true;

   
    GameObject obj;
    void Start()
    {
        if(mText ==null)
        mText = GameObject.Find("SayText").GetComponent<Text>();
    }

    void Update()
    {
        //从角色位置向NPC发射一条经过鼠标位置的射线  
        Ray mRay = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit mHi;

        //判断是否击中了NPC  
        if (Physics.Raycast(mRay, out mHi))//如果射线又遇到物体
        {
            // Debug.Log(mHi.collider.gameObject.tag);
            //如果击中了NPC  
            if (isFirst)
            {
                if (mHi.collider.gameObject == this.gameObject)
                {
                    if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
                    {
                        
                        mText.transform.parent.gameObject.SetActive(true);
                        baoxiangNUM++;
                        if (baoxiangNUM == bxNUM)
                        {
                            if (this.gameObject.tag == "bxNPC")
                                mText.text = this.gameObject.name + ":我现在送你给你一个宝箱！继续冒险吧！骚年！你的宝箱收集满了呢！去和中间大门处的那个女生对话继续吧！";
                            else
                            mText.text = "你找到了所有的宝箱！目前获得的宝箱：" + baoxiangNUM + "/5，你可以和在你门口NPC对话完成了！";
                        }
                        else
                        {
                            if (this.gameObject.tag == "bxNPC")
                                mText.text = this.gameObject.name + ":我现在送你给你一个宝箱！继续冒险吧！骚年！你现在有着宝箱：" + baoxiangNUM + "/" + bxNUM; 
                            else
                            mText.text = "你找到了一个宝箱！目前获得的宝箱：" + baoxiangNUM + "/" + bxNUM;
                        }
                        
                        isFirst = false;
                        StartCoroutine(closesay());

                    }
                }
            }
            

        }
        

    }

    private IEnumerator closesay()
    {
        yield return new WaitForSeconds(2.0f);
        if (isFirst2)
        {
            mText.transform.parent.gameObject.SetActive(false);
            isFirst2 = false;
            this.gameObject.SetActive(false);
        }
    }
   
}
