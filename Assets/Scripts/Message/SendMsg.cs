﻿using UnityEngine;
using System.Collections;

public class SendMsg : MonoBehaviour {
    public static string ActionMsgSend(MsgLightState temp)
    {
        string t = "";
        t += "#";
        t += "1A";
        t += "LED";
        t += temp.L1.ToString("000");
        t += temp.L2.ToString("000");
        t += temp.L3.ToString("000");
        t += temp.L4.ToString("000");
        t += temp.L5.ToString("000");
        t += temp.L6.ToString("000");
        t += "68";
        return t;
    }
    public string ActionMsgSend(MsgHandControl temp)
    {
        string t = "";
        t += "*";
        t += ((char)30).ToString();
        t += "1";
        t += temp.M1;
        t += "%";
        t += temp.M2;
        t += "%";
        t += temp.M3;
        t += "%";
        t += temp.M4;
        t += "%";
        t += temp.M5;
        t += "%";
        t += temp.M6;
        t += "%";
        t += "^";
        t += "20";
        return t;
    }
    public string ActionMsgSend(MsgHandleControl temp)
    {
        string t = "";
        t += "*";
        t += ((char)7).ToString();
        t += temp.Mode;
        t += temp.Control;
        t += "^";
        t += "20";
        return t;
    }
    public string ActionMsgSend(MsgHandle_Control temp)
    {
        string t = "";
        t += "*";
        t += ((char)14).ToString();
        t += temp.Mode;
        t += temp.H;
        t += temp.H_O;
        t += temp.V;
        t += temp.V_O;
        t += "^";
        t += "20";
        return t;
    }
    public string ActionMsgSend(MsgLEDControl temp)
    {
        string t = "";
        t += "~";
        t += ((char)30).ToString();
        t += "1";
        t += temp.L1.ToString("000");
        t += "%";
        t += temp.L2.ToString("000");
        t += "%";
        t += temp.L3.ToString("000");
        t += "%";
        t += temp.L4.ToString("000");
        t += "%";
        t += temp.L5.ToString("000");
        t += "%";
        t += temp.L6.ToString("000");
        t += "%";
        t += "^";
        t += "20";
        return t;
    }
    public string ActionMsgSend(MsgMACC temp)
    {
        string t = "";
        t += "~";
        t += ((char)7).ToString();
        t += temp.Type;
        t += temp.Control;
        t += "^";
        t += "20";
        return t;
    }
   
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}
}
