﻿
public class MsgROVState  //1.1航行器基本状态
{
    public byte Head;
    public int Length;
    public string rovState;
    public float deviationAG;
    public float UpdownAG;
    public float RollAG;
    public float XASpeed;
    public float YASpeed;
    public float ZASpeed;
    public float Depth;
    public short Temper;
    public short Humi;
    public double Lng;
    public double Lat;
    public short Whirl;
    public short Battery;
    public string State;
    public string CRC;
};

public class MsgLightState  //1.2灯光状态
{
    public byte Head;
    public int Length;
    public string LightType;
    public double L1;
    public double L2;
    public double L3;
    public double L4;
    public double L5;
    public double L6;
    public string CRC;
};

public class MsgHandControl  //2.1.1手动调试
{
    public byte Head;
    public int Length;
    public string Mode;
    public double M1;
    public double M2;
    public double M3;
    public double M4;
    public double M5;
    public double M6;
    public string Tail;
    public string CRC;
};

public class MsgHandleControl  //2.1.2运行模式（手柄控制）
{
    public byte Head;
    public int Length;
    public string Mode;
    public string Control;
    public string Tail;
    public string CRC;
};
public class MsgHandle_Control  //2.1.2（手柄控制）
{
    public byte Head;
    public int Length;
    public string Mode="2";
    public string H="0";
    public string H_O="000";
    public string V="0";
    public string V_O="000";
    
    public string Tail;
    public string CRC;
};

public class MsgAutoReturn  //2.1.3下位机发送岸上机位置（只有在自动回航时使用）
{
    public byte Head;
    public int Length;
    public string Mode;
    public string GPSinstruction;
    public double Lng;
    public double Lat;
    public string Sonar;
    public double Xwater;
    public double Ywater;
    public double Zwater;
    public string Tail;
    public string CRC;
};

public class MsgLEDControl //2.2 LED灯指令
{
    public byte Head;
    public int Length;
    public string Type;
    public double L1=0;
    public double L2=0;
    public double L3=0;
    public double L4=0;
    public double L5=0;
    public double L6=0;
    public string Tail;
    public string CRC;
};

public class MsgMACC  //机械臂、云台、履带车
{
    public byte Head;
    public int Length;
    public string Type;
    public string Control;
    public string Tail;
    public string CRC;
};