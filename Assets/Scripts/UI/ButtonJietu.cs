﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
public class ButtonJietu : MonoBehaviour
{
    public Camera VideoCamera;
    //public Camera uiCamera;
    void Start()
    {
        //btn.onClick.AddListener(screen);
    }

    // Update is called once per frame
    void Update()
    {

    }

    private IEnumerator CaptureByCamera(Camera mCamera, Rect mRect)
    {
        //等待渲染线程结束  
        yield return new WaitForEndOfFrame();

        //初始化RenderTexture  
        RenderTexture mRender = new RenderTexture((int)mRect.width, (int)mRect.height, 0);
        //设置相机的渲染目标  
        mCamera.targetTexture = mRender;
        //开始渲染  
        mCamera.Render();

        //激活渲染贴图读取信息  
        RenderTexture.active = mRender;

        Texture2D mTexture = new Texture2D((int)mRect.width, (int)mRect.height, TextureFormat.RGB24, false);
        //读取屏幕像素信息并存储为纹理数据  
        mTexture.ReadPixels(mRect, 0, 0);
        //应用  
        mTexture.Apply();

        //释放相机，销毁渲染贴图  
        mCamera.targetTexture = null;
        RenderTexture.active = null;
        GameObject.Destroy(mRender);

        //将图片信息编码为字节信息  
        byte[] bytes = mTexture.EncodeToPNG();
        //保存  
        string filename = Application.dataPath + "/Imgs/Img" + System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".png";
        System.IO.File.WriteAllBytes(Application.dataPath + "/Imgs/" + System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".png", bytes);
        Debug.Log(string.Format("截屏了一张照片: {0}", filename));

        //如果需要可以返回截图  
        //return mTexture;  
    }

    private void AllKit()
    {
        string filename = Application.dataPath + "/Imgs/Img" + System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss") + ".png";
        Application.CaptureScreenshot(filename, 0);
        Debug.Log(string.Format("截屏了一张照片: {0}", filename));
    }

    public void click()
    {
        AllKit();
        //StartCoroutine(CaptureByCamera(VideoCamera, new Rect(0, 0, Screen.width, Screen.height)));
        //CaptureCamera(VideoCamera, new Rect(0, 0, Screen.width, Screen.height));
    }
}
