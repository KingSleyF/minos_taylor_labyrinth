﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainButton : MonoBehaviour {

    public AudioListener AL;
    bool AlPlay = true;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void TuiChu()
    {
        Application.Quit();
    }

    public void YinYue()
    {
        if (AL != null)
        {
            if (AlPlay)
            {
                AL.enabled = false;
                AlPlay = false;
            }
            else
            {
                AL.enabled = true;
                AlPlay = true;
            }
        }
    }
}
