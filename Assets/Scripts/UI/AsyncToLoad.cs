﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class AsyncToLoad : MonoBehaviour {
    
    public GameObject loadingBar;           // 进度条对象  
    Slider slider;                        // 进度条脚本  
    Text txet_Percentage;               // 显示进度的百分比的标签  

    // Use this for initialization  
    void Start()
    {
        slider = loadingBar.GetComponent<Slider>();
        txet_Percentage = GameObject.Find("txet_Percentage").GetComponent<Text>();
        LoadScene();
    }

    // 调用加载界面的加载协程  
    public void LoadScene()
    {
        string name = "main";
        StartCoroutine(StartLoading(name));//StartLoading后面括号填的是要加载的界面的名字，我这里填的是我自己的界面名字加载，使用的话，记得设置为你自己的界面名字比如StartCoroutine(StartLoading("Level1"));  
    }

    // 异步加载协同程序  
    IEnumerator StartLoading(string name)
    {

        int displayProgress = 0;                // 进度条显示值  
        int toProgress = 0;                     // 进度条要达到的值  
        AsyncOperation aop = SceneManager.LoadSceneAsync(name);
        aop.allowSceneActivation = false;           // 设置不允许加载完成就进入界面  
        // 判断是否进度小于0.9，如果是的话循环输出进度条的值  
        while (aop.progress < 0.9f)
        {
            toProgress = (int)(aop.progress * 100);
            while (displayProgress < toProgress)
            {
                ++displayProgress;
                SetLoadingPercentage(displayProgress);
            }
            yield return new WaitForEndOfFrame();// 每次循环结束执行完当前帧才继续下一个循环  
        }
        // Unity就只会加载场景到90%，剩下的10%要等到allowSceneActivation设置为true后才加载，所以设置增加进度条效果，让进度条平滑显示达到100%  
        toProgress = 100;
        while (displayProgress < toProgress)
        {
            ++displayProgress;
            SetLoadingPercentage(displayProgress);
            yield return new WaitForEndOfFrame();
        }

        yield return new WaitForSeconds(0.2f);//延缓进入游戏时间，如果场景较小加载很快，为了看到进度效果，做了个小小延迟，不需要的可以去掉  
        aop.allowSceneActivation = true;            // 设置加载完成可以进入界面  
    }

    // 设置加载进度条信息  
    void SetLoadingPercentage(int _value)
    {
        slider.value = _value / 100.0f;
        txet_Percentage.text = _value + "%";
    }
}
