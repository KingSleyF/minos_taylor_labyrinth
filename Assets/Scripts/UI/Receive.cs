﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System;
using System.Runtime.InteropServices;
using System.Text;
using System.IO;
using System.Xml;

public class Receive : MonoBehaviour
{
    private string currentButton;//当前按下的按键
    
    public Text SayingText;
    string sendend = " ";

    private SendMsg Send_Msg = new SendMsg();

    private MsgROVState m_rovData = new MsgROVState();
    private MsgLightState m_ledData = new MsgLightState();
    private MsgHandControl m_manualOrder = new MsgHandControl();
    private MsgHandleControl m_HSOrder = new MsgHandleControl();
    private MsgHandle_Control m_JK = new MsgHandle_Control();
    private MsgAutoReturn m_autoReturnOrder = new MsgAutoReturn();
    private MsgLEDControl m_ledControl = new MsgLEDControl();
    private MsgMACC m_macc;

    private bool Judge_Net_Send = true;

    void Awake()
    {
        System.IO.Directory.CreateDirectory(Application.dataPath + "/Imgs/");
        System.IO.Directory.CreateDirectory(Application.dataPath + "/Vedios/");
    }

    // Use this for initialization
    void Start()
    {
        SayingText.text = sendend;
    }

    void Update()
    {
        judge_net();
    }

    //网络连接判断
    public void judge_net()
    {
        if (NetServer.flag == true)
        {
            if (Judge_Net_Send)
            {
                string a = "iap_jump_app";
                NetServer.server_DatagramSend(a);
                Judge_Net_Send = false;
            }
        }
        else
            Judge_Net_Send = true;
    }

    //发送对话框信息
    public void SendClick()
    {
        if (SayingText.text != " ")
        {
            string a = SayingText.text;
            SayingText.text = sendend;
            NetServer.server_DatagramSend(a);
        }
    }

    void Send_LED()
    {
        string a = Send_Msg.ActionMsgSend(m_ledControl);
        NetServer.server_DatagramSend(a);
        Debug.Log(m_ledControl.L2 + "222222");
    }

    void Send_HANDLE()
    {
        string a = Send_Msg.ActionMsgSend(m_HSOrder);
        NetServer.server_DatagramSend(a);
        Debug.Log("333333");
    }
    
    private float gougu(float a,float b)
    {
        float c = (float)Math.Sqrt(Math.Pow(a, 2) + Math.Pow(b, 2));
        c = c / (127f * (float)Math.Sqrt(2)) * 127f;
        return c;
    }
}
