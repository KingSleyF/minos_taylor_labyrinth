﻿using UnityEngine;
using System.Collections;

public class MonsterData
{
    public int id;
    public float fireRange;
    public float intonateTime;
    public float walkSpeed;
    public int maxHp;
    public int curHp;


    public MonsterData()
    {
        id = 0;
        fireRange = 1.0f;
        intonateTime = 3.0f;
        walkSpeed = 3.0f;
        maxHp = 10;
        curHp = 10;
    }
}

public class Monster : MonoBehaviour
{
    private Animation m_anim;
    public AudioClip m_DeadAudioClip;
    public AudioClip m_HitAudioClip;
    public AudioClip m_BeatAudioClip;
    public AudioClip m_WalkAudioClip;
    private MonsterData m_mosterData = new MonsterData();
    public MonsterData MonsterData
    {
        get { return m_mosterData; }
        set { m_mosterData = value; }
    }
    void Awake()
    {
        UnityEngine.AI.NavMeshHit closestHit;
        if (UnityEngine.AI.NavMesh.SamplePosition(this.transform.position, out closestHit, 500, 1))
        {
            this.transform.position = closestHit.position;
            UnityEngine.AI.NavMeshAgent agent = this.GetComponent<UnityEngine.AI.NavMeshAgent>();
            if (agent != null)
            {
                agent.speed = m_mosterData.walkSpeed;
            }
        }
    }
    public Transform TargetObject = null;
    public float atkdistance = 0.9f;
    private Vector3 m;
    private Vector3 n;
    private float x;
    void Start()
    {
        m_anim = this.GetComponent<Animation>();
        TargetObject = Player.MainPlayer.GetComponent<Transform>();
    }
    void Update()
    {
        m = transform.position;
        n = TargetObject.transform.position;
        x = Vector3.Distance(m, n);

        if (TargetObject != null && x > atkdistance)
        {
            this.GetComponent<UnityEngine.AI.NavMeshAgent>().destination = TargetObject.position;
            this.transform.LookAt(TargetObject);
        }
    }

    public void Init(int id)
    {
        m_mosterData.id = id;
    }

    public void OnHurt(int hp)
    {
        m_mosterData.curHp -= hp;
    }
    public void MonsterAnimation(string clip)
    {
        if (m_anim != null)
            m_anim.CrossFade(clip);
    }
}