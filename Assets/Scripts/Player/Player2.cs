﻿using UnityEngine;
using System.Collections;
#region 寻路组件
/* 
public class Player : MonoBehaviour
{

    NavMeshAgent m_NavAgent;
    Animation m_anim;
    // Use this for initialization
    void Start()
    {
        m_NavAgent = this.GetComponent<NavMeshAgent>();
        m_anim = this.GetComponent<Animation>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            LayerMask mask = 1 << 9;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask))
            {
                this.transform.LookAt(hit.point);
                m_NavAgent.SetDestination(hit.point);
                m_anim.Play("run");
            }

        }
    }
}
*/
#endregion



public class Player2 : MonoBehaviour
{
    public static Player2 MainPlayer = null;
    private UnityEngine.AI.NavMeshAgent m_NavAgent;
    private Animation m_anim;
	private bool Key = true;
	public void Stop()
	{
		Key = false;
	}
	public void Resume()
	{
		Key = true;
	}



    void Awake()
    {
        MainPlayer = this;
    }
    // Use this for initialization
    void Start()
    {
        m_NavAgent = this.GetComponent<UnityEngine.AI.NavMeshAgent>();
		m_anim = this.GetComponent<Animation>();

    }

    // Update is called once per frame
    void Update()
    {
		
		if (PlayerData.Player_curHp > 0 && Key) {
			if (Input.GetMouseButton (0)) {
				
				//Debug.Log(Input.mousePosition);
				RaycastHit hit;
				LayerMask mask = 1 << 9;
				Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
				Debug.DrawLine (ray.origin, Input.mousePosition, Color.red);
				if (Physics.Raycast (ray, out hit, Mathf.Infinity, mask)) {
					this.transform.LookAt (hit.point);
					m_NavAgent.SetDestination (hit.point);
					Debug.DrawLine (ray.origin, hit.point, Color.red);
				}
			}
		}
		if ( PlayerData.Player_curHp < PlayerData.Player_maxHp ) 
		{
			if (Input.GetMouseButtonDown (1)) 
			{
				PlayerData.Player_curHp += 10;
				/*if (PlayerData.NowHP > PlayerData.MaxHP)
				{
					PlayerData.NowHP = PlayerData.MaxHP;
				}*/
			}
		}


    }
    public void PlayerAnimation(string clip)
    {
		if (m_anim != null) {
			m_anim.CrossFade (clip);
		}
    }


}
