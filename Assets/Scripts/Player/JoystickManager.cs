﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JoystickManager : MonoBehaviour
{
    private string currentButton;//当前按下的按键
    public float speed = 1.2f;
    public float maxspeed = 2.6f;

    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void Update () {
        JK();
    }

    private void JK()
    {
        var values = Enum.GetValues(typeof(KeyCode));//存储所有的按键  
        for (int x = 0; x < values.Length; x++)
        {
            if (Input.GetKeyDown((KeyCode)values.GetValue(x)))
            {
                currentButton = values.GetValue(x).ToString();//遍历并获取当前按下的按键  
                //Debug.Log(currentButton);
            }
        }

        if (Input.GetKeyUp(KeyCode.JoystickButton0))
        {
            //获取到键并抬起后的操作
            Debug.Log("Y上键");
        }
        if (Input.GetKeyUp(KeyCode.JoystickButton1))
        {
            //获取到键并抬起后的操作
            Debug.Log("B右键");
        }
        if (Input.GetKeyUp(KeyCode.JoystickButton2))
        {
            //获取到键并抬起后的操作
            Debug.Log("A下键");
        }
        if (Input.GetKeyUp(KeyCode.JoystickButton3))
        {
            //获取到键并抬起后的操作
            Debug.Log("X左键");
        }
        if (Input.GetKeyUp(KeyCode.JoystickButton4))
        {
            //获取到键并抬起后的操作
            Debug.Log("LT/L1左上键");
        }
        if (Input.GetKeyUp(KeyCode.JoystickButton5))
        {
            //获取到键并抬起后的操作
            Debug.Log("RT/R2右上键");
        }
        if (Input.GetKeyUp(KeyCode.JoystickButton6))
        {
            //获取到键并抬起后的操作
            Debug.Log("LB/L2左下键");
        }
        if (Input.GetKeyUp(KeyCode.JoystickButton7))
        {
            //获取到键并抬起后的操作
            Debug.Log("RB/R2右下键");
        }
        if (Input.GetAxis("Xbox L X") < 0 && Input.GetAxis("Xbox L Y") == 0)
        {
            //十字键左
            Debug.Log("左摇杆左");
            this.transform.Rotate(0, -3, 0);
        }
        if (Input.GetAxis("Xbox L Y") > 0 && Input.GetAxis("Xbox L X") == 0)
        {
            //十字键上
            Debug.Log("左摇杆上");
            gameObject.GetComponent<Player>().PlayerAnimation("run");
            this.transform.Translate(speed * Vector3.forward * Time.deltaTime);
            if (speed < maxspeed)
                speed += Time.deltaTime;
        }
        else
        {
            if (!(Input.GetAxis("Xbox L Y") < 0 && Input.GetAxis("Xbox L X") == 0))
                gameObject.GetComponent<Player>().PlayerAnimation("idle");
            speed = 3.0f;
        }
        if (Input.GetAxis("Xbox L X") > 0 && Input.GetAxis("Xbox L Y") == 0)
        {
            //十字键右
            Debug.Log("左摇杆右");
            this.transform.Rotate(0, 3, 0);
        }
        if (Input.GetAxis("Xbox L Y") < 0 && Input.GetAxis("Xbox L X") == 0)
        {
            //十字键下
            Debug.Log("左摇杆下");
            gameObject.GetComponent<Player>().PlayerAnimation("walk");
            this.transform.Translate(1.2f * Vector3.back * Time.deltaTime);
        }
        if (Input.GetAxis("Xbox L X") > 0 && Input.GetAxis("Xbox L Y") > 0)
        {
            //十字键右上
            Debug.Log("左摇杆右上");
        }
        if (Input.GetAxis("Xbox L X") < 0 && Input.GetAxis("Xbox L Y") > 0)
        {
            //十字键左上
            Debug.Log("左摇杆左上");
        }
        if (Input.GetAxis("Xbox L X") < 0 && Input.GetAxis("Xbox L Y") < 0)
        {
            //十字键左下
            Debug.Log("左摇杆左下");
        }
        if (Input.GetAxis("Xbox L X") > 0 && Input.GetAxis("Xbox L Y") < 0)
        {
            //十字键右下
            Debug.Log("左摇杆右下");
        }
        if (-1 <= Input.GetAxis("Xbox R X") && Input.GetAxis("Xbox R X") < 0)
        {
            //右摇杆左
            Debug.Log("右摇杆左");
        }
        if (-1 <= Input.GetAxis("Xbox R Y") && Input.GetAxis("Xbox R Y") < 0)
        {
            //右摇杆上
            Debug.Log("右摇杆上");
        }

        if (0 < Input.GetAxis("Xbox R X") && Input.GetAxis("Xbox R X") <= 1)
        {
            //右摇杆右
            Debug.Log("右摇杆右");
        }
        if (0 < Input.GetAxis("Xbox R Y") && Input.GetAxis("Xbox R Y") <= 1)
        {
            //右摇杆下
            Debug.Log("右摇杆下");
        }
    }
}
