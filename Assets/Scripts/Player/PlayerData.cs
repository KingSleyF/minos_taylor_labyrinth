﻿using UnityEngine;
using System.Collections;

public class PlayerData 
{
    public static float SpeedWalk = 4.0f;
    public static float SpeedRun = 12.0f;
    public static float AttackDistance = 4f;
    public static float AttackAngle1 = 45.0f;
    public static float AttackDistance2 = 4f;
    public static float AttackAngle2 = 60.0f;
    public static int Player_maxHp = 20;
    public static int Player_curHp = 20;
    public static int Beat_Count = 0;
}
