﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour
{
    public float speed =1.2f;
    public float maxspeed = 2.6f;
    bool yizhi;

    public float angle = 3.0f;
    public static Player MainPlayer = null;
    private UnityEngine.AI.NavMeshAgent m_NavAgent;
    public Animation m_anim;
    private bool m_DoubleClick = false;
    private PlayerStateManager m_StateManager;
    private AvatarTarget m_AttackTarget;
    public GameObject m_Object;
    public AudioClip m_WalkAudioClip;
    public AudioClip m_RunAudioClip;
    public AudioClip m_HitAudioClip;
    public AudioClip m_AttackAudioClip;
    public AudioClip m_DieAudioClip;

    Vector3 oldplace;
    Vector3 newplace;
    Vector3 speedv;
    float timer;

    void Awake()
    {
        MainPlayer = this;
        timer = 0;
        yizhi = false;
    }
    // Use this for initialization
    void Start()
    {
        m_NavAgent = this.GetComponent<UnityEngine.AI.NavMeshAgent>();
        m_StateManager = this.GetComponent<PlayerStateManager>();
        m_anim = this.GetComponent<Animation>();
        m_Object = this.GetComponent<GameObject>();
        newplace = this.transform.position;
    }

    void OnTriggerEnter(Collider collider)
    {
        if (collider.tag == "attack")
        //if (collider.gameObject.name == "kulou_gangti" || collider.gameObject.name == "sheyao_gongti")
        {
            //Debug.Log("TriggerEnter");
            //Debug.Log(PlayerData.Plauer_curHp);
            if (m_StateManager.GetCurrentStateID() != StateID.eStateID_Object_Hurt && m_StateManager.GetCurrentStateID() != StateID.eStateID_Object_Run && m_StateManager.GetCurrentStateID() != StateID.eStateID_Object_Attack)
            //if (PlayerData.Plauer_curHp > 0)
            {
                m_Object = collider.gameObject;
                m_StateManager.SetTransition(Transition.eTransiton_Object_Hurt);
            }
        }
    }
    // Update is called once per frame
    void Update()
    {
        if(!atk())
            move();
       
        //if (Input.GetMouseButtonDown(0) && PlayerData.Player_curHp > 0)
        //{
        //    if (m_DoubleClick) 
        //    {
        //        RaycastHit hit;
        //        LayerMask mask = 1 << 9;
        //        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                
        //        if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask))
        //        {
        //            this.transform.LookAt(hit.point);
        //        }
        //        m_StateManager.SetTransition(Transition.eTransiton_Object_Run);
        //        m_DoubleClick = false;
        //    }
        //    else 
        //    {
        //        m_DoubleClick = true;
        //        StartCoroutine(CheckDoubleClick(0.5f));
        //    }
        //    if (m_StateManager.GetCurrentStateID() != StateID.eStateID_Object_Run)
        //    {
        //        RaycastHit hit;
        //        LayerMask mask = 1 << 9;
        //        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //        if (Physics.Raycast(ray, out hit, Mathf.Infinity, mask))
        //        {
        //            this.transform.LookAt(hit.point);
        //            m_NavAgent.SetDestination(hit.point);
        //            Debug.DrawLine(ray.origin, hit.point, Color.red);
        //        }
        //    }
        //}
    }

    IEnumerator CheckDoubleClick(float nu)
    {
        yield return new WaitForSeconds(nu);
        m_DoubleClick = false;

    }

    public void PlayerAnimation(string clip)
    {
        
        if (m_anim != null)
            if(!m_anim.IsPlaying(clip))

                m_anim.CrossFade(clip);
        
    }

    void move()
    {
        
        if (Input.GetKey("w"))
        {
            PlayerAnimation("run");
            this.transform.Translate(speed * Vector3.forward * Time.deltaTime);
            if (speed < maxspeed)
                speed += Time.deltaTime;
        }
        else
        {
            if (!Input.GetKey("s"))
                PlayerAnimation("idle");
            speed = 3.0f;
        }
        if (Input.GetKey("s"))
        {
            PlayerAnimation("walk");
            this.transform.Translate(1.2f * Vector3.back * Time.deltaTime);
        }
        if (Input.GetKey("a"))
            this.transform.Rotate(0,-angle,0);
        if (Input.GetKey("d"))
            this.transform.Rotate(0,angle, 0);
    }
    bool atk()
    {
        if (Input.GetKey("j"))
        {
            PlayerAnimation("attack02");
            return true;
        }

        else
        {
            if (m_anim.IsPlaying("attack02"))
                return true;
            else
            return false; }

       // this.transform.LookAt(this.transform.position+speedv);

    }
    

}