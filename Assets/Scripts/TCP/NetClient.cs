﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System;
using UnityEngine.UI;

public class NetClient : MonoBehaviour {

    public Text SayingText;
    string sendend = " ";

    // Use this for initialization
    void Start () 
    {
        SayingText.text = sendend;
        Init();
    }
	
	// Update is called once per frame
	void Update () {
        try
        {
         
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
        }
	}

    //void OnGUI()
    //{
    //    if (GUILayout.Button("登录"))
    //    {
    //        Init();
    //    }
    //    if (GUILayout.Button("发送消息"))
    //    {
    //        if (client != null && client.Connected)
    //        {
    //         //   string message = "#23YA180.55PI-90.55RO050.55AX000.00AY000.00AZ000.00DE100.00TE55HU33E1923084N2606453TW201268";
    //           string message = "#65ROVYA180.55PI-90.55RO050.55AX100.01AY000.02AZ000.03DE100.00TE55HU33E1923084N2606453TW20BA100ST1268";
    //            //将字符串转化为字符数组
    //           //Encoding.ASCII.GetBytes(message);
    //            Byte[] bytes = System.Text.Encoding.Default.GetBytes(
    //            message.ToCharArray());
    //            client.Send(bytes);
    //        }
    //    }
    //}

    static AsyncTcpClient client;

    static void Init()
    {
        //   LogFactory.Assign(new ConsoleLogFactory());

        // 测试用，可以不指定由系统选择端口
        IPEndPoint remoteEP = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1400);
        IPEndPoint localEP = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 9998);
        client = new AsyncTcpClient(remoteEP, localEP);
        client.Encoding = Encoding.UTF8;
        client.ServerExceptionOccurred +=
          new EventHandler<TcpServerExceptionOccurredEventArgs>(client_ServerExceptionOccurred);
        client.ServerConnected +=
          new EventHandler<TcpServerConnectedEventArgs>(client_ServerConnected);
        client.ServerDisconnected +=
          new EventHandler<TcpServerDisconnectedEventArgs>(client_ServerDisconnected);
        client.PlaintextReceived +=
          new EventHandler<TcpDatagramReceivedEventArgs<string>>(client_PlaintextReceived);
        client.Connect();

        Debug.Log("TCP client has connected to server.");
        Debug.Log("Type something to send to server...");
    }

    static void client_ServerExceptionOccurred(
      object sender, TcpServerExceptionOccurredEventArgs e)
    {
         Debug.Log(string.Format("TCP server {0} exception occurred, {1}.",
             e.ToString(), e.Exception.Message));
    }

    static void client_ServerConnected(
      object sender, TcpServerConnectedEventArgs e)
    {
        Debug.Log(string.Format( "TCP server {0} has connected.", e.ToString()));
    }

    static void client_ServerDisconnected(
      object sender, TcpServerDisconnectedEventArgs e)
    {
        Debug.Log(string.Format( "TCP server {0} has disconnected.", e.ToString()));
    }

    static void client_PlaintextReceived(
      object sender, TcpDatagramReceivedEventArgs<string> e)
    {
        Debug.Log(string.Format("Server : {0} --> ",
          e.TcpClient.Client.RemoteEndPoint.ToString()));
        Debug.Log(string.Format("{0}", e.Datagram));
    }

    //发送对话框信息
    public void SendClick()
    {
        if (SayingText.text != " ")
        {
            string a = SayingText.text;
            SayingText.text = sendend;
            client.Send(a);
        }
    }
}
