﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using System.Collections;
using UnityEngine;

public class SProtocol
{
    private int index = 0;
    private int length = 0;
    private string content = null;
    public SProtocol()
    {
        length = 0;
        index = 0;
    }
    public int Length
    {
        //set
        //{
        //    String.Copy(content);
        //}
        get
        {
            return length;
        }
    }
    public int Index
    {
        get { return index; }

    }
    public string data
    {
        get
        {
            return content;
        }
    }
    //private void CheckLength(int length)
    //{
    //    if (index + length > content.Length)
    //    {
    //        byte[] temp = new byte[(index + length) * 2];
    //        Array.Copy(content, temp, content.Length);
    //        content = temp;
    //    }
    //}
    public void Bind(string value, int len)
    {
        //CheckLength(len);
        content = String.Copy(value);
        length = len;
    }
    //public void Bind(byte[] value, int start, int len)
    //{
    //    CheckLength(len);
    //    Array.Copy(value, start, content, 0, len);
    //    length = (short)len;
    //}
    public void PushName(string name)
    {
        index = 0;
        PushUChar(name);
    }
    public byte PopName()
    {
        index = 0;
        return System.Text.Encoding.Default.GetBytes ( PopUChar() )[0];
    }
    public void PushUChar(string value)
    {
        //CheckLength(1);
        //content[index] = value;
        value.Substring(index, 1);
        index += 1;
        length = (short)index;
    }
    public string PopUChar()
    {
        if (index >= length) return null;
        string ret = content.Substring(index,1);
        index += 1;
        return ret;
    }
    //public void PushInt16(short value)
    //{
    //    //CheckLength(2);
    //    //byte[] temp = BitConverter.GetBytes(value);
    //    //Array.Copy(temp, 0, content, index, temp.Length);
    //    int temp = Convert.ToInt32(content.Substring(1, 2), 16);
    //    index += 2;
    //    length = (short)index;
    //}

    public static string SChange2(string s) //单字符变换二进制
     {
         byte[] data = Encoding.Unicode.GetBytes(s);
         StringBuilder result = new StringBuilder(data.Length * 8);
         byte b = data[0];
         //foreach (byte b in data)
             result.Append(Convert.ToString(b, 2).PadLeft(8, '0'));
         return result.ToString();
     }

    public short PopInt16()
    {
        if (index >= length) return 0;
        int ret = Convert.ToInt32(content.Substring(index, 2), 16);
        index += 2;
        return (short)ret;
    }
    
    public short onePopInt16()
    {
        if (index >= length) return 0;
        int ret = Convert.ToInt32(content.Substring(index, 1), 16);
        index += 1;
        return (short)ret;
    }
    public string PopBytes1()
    {
        string res = "";
        if (index >= length) return res;
        res = content.Substring(index, 1);
        index += 1;
        return res;
    }
    public short PopBytes2()
    {
        short res = 0;
        if (index >= length) return res;
        //string msgStr = Encoding.Default.GetString(content, 0, length);
        res = short.Parse(content.Substring(index, 2));
        index += 2;
        return res;
    }
    public string PopBytes3()
    {
        string res = "";
        if (index >= length) return res;
        //string msgStr = Encoding.Default.GetString(content,0, length);
        res = content.Substring(index, 3);
        index += 3;
        return res;
    }

    public float PopBytes2_6()
    {
        float res = 0;
        if (index >= length) return res;
        //string msgStr = Encoding.Default.GetString(content, 0, length);
        res = float.Parse(content.Substring(index + 2, 6));
        index += 8;
        return res;
    }
    public float PopBytes2_7()
    {
        float res = 0;
        if (index >= length) return res;
        //string msgStr = Encoding.Default.GetString(content, 0, length);
        res = float.Parse(content.Substring(index + 2, 7));
        index += 9;
        return res;
    }
   
    public short PopBytes2_2()
    {
        short res = 0;
        if (index >= length) return res;
        //string msgStr = Encoding.Default.GetString(content, 0, length);
        res = short.Parse(content.Substring(index + 2, 2));
        index += 4;
        return res;
    }
    public string sPopBytes2_2()
    {
        string res = "";
        string res1;
        string res2;
        if (index >= length) return res;
        //string msgStr = Encoding.Default.GetString(content, 0, length);
        res1 = content.Substring(index + 2, 1);
        res2 = content.Substring(index + 3, 1);
        res1 = SChange2(res1);
        res2 = SChange2(res2);
        res = res1 + res2;
        index += 4;
        return res;
    }
    public short PopBytes2_3()
    {
        short res = 0;
        if (index >= length) return res;
        //string msgStr = Encoding.Default.GetString(content, 0, length);
        res = short.Parse(content.Substring(index + 2, 3));
        index += 5;
        return res;
    }
    public double PopBytes1_7()
    {
        double res = 0;
        if (index >= length) return res;
        //string msgStr = Encoding.Default.GetString(content, 0, length);
        res = double.Parse(content.Substring(index + 1, 7));
        index += 8;
        return res;
    }
    public float PopBytes0_3()
    {
        float res = 0;
        if (index >= length) return res;
        //string msgStr = Encoding.Default.GetString(content, 0, length);
        res = float.Parse(content.Substring(index, 3));
        index += 3;
        return res;
    }
    public string PopBytes1_1()
    {
        string res = "";
        if (index >= length) return res;
        //string msgStr = Encoding.Default.GetString(content, 0, length);
        res = content.Substring(index + 1, 1);
        index += 1;
        return res;
    }
    public string PopBytes1_2()
    {
        string res = "";
        string res1;
        string res2;
        if (index >= length) return res;
        //string msgStr = Encoding.Default.GetString(content, 0, length);
        res1 = content.Substring(index + 1, 1);
        res2 = content.Substring(index + 2, 1);
        res1 = SChange2(res1);
        res2 = SChange2(res2);
        res = res1 + res2;
        res = content.Substring(index, 2);
        index += 2;
        return res;
    }
    public float PopBytes1_3()
    {
        float res = 0;
        if (index >= length) return res;
        //string msgStr = Encoding.Default.GetString(content, 0, length);
        res = float.Parse(content.Substring(index, 3));
        index += 3;
        return res;
    }
    public string PopBytes0_2()
    {
        string res = "";
        string res1;
        string res2;
        if (index >= length) return res;
        //string msgStr = Encoding.Default.GetString(content, 0, length);
        res1 = content.Substring(index, 1);
        res2 = content.Substring(index + 1, 1);
        res1 = SChange2(res1);
        res2 = SChange2(res2);
        res = res1 + res2;
        res = content.Substring(index, 2);
        index += 2;
        return res;
    }
}
