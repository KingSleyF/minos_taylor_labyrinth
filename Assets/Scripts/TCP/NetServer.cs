﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System;
using System.Xml;
using System.IO;

public class NetServer : MonoBehaviour
{
    private static AsyncTcpServer m_server = null;
    private static Queue<SProtocol> m_protoQueue = null;
    private static List<SProtocol> m_removeProtoList;
    private static Dictionary<byte, TcpDispatchEvent> m_dispatchFunctions;
    public static int ListenPort = 1400;
    public static string re;
    public static string se;
    public static string tim = DateTime.Now.ToString();
    // Use this for initialization
    public static bool flag = false;
    void Start()
    {
        m_removeProtoList = new List<SProtocol>();
        m_dispatchFunctions = new Dictionary<byte, TcpDispatchEvent>();
        re = Application.dataPath + "\\Receive.txt";
        se = Application.dataPath + "\\Send.txt";
        LoadXML();
        Init();
    }
    void Destroy()
    {
        if (m_server != null)
        {
            m_server.Stop();
        }
    }
    /*
  * 加载配置文件
  */
    private void LoadXML()
    {
        string path = Application.dataPath + "/Data.xml";
        if (File.Exists(path))
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(path);
            XmlNodeList node = doc.SelectSingleNode("Version").ChildNodes;
            XmlNodeList xe = node.Item(node.Count - 1).ChildNodes;
            foreach (XmlElement element in xe)
            {
                if (element.Name.Equals("ListenPort"))
                {
                    ListenPort = Convert.ToInt32(element.InnerText);
                    Debug.Log("ListenPort:" + ListenPort);
                }
            }
        }
        else
            Debug.Log("无路径");
    } 

    // Update is called once per frame
    void FixedUpdate()
    {
        if (m_server != null && m_server.IsRunning)
        {
            if (m_protoQueue.Count > 0)
            {
                lock (m_protoQueue)
                {
                    while (m_protoQueue.Count > 0)
                    {
                        m_removeProtoList.Add(m_protoQueue.Dequeue());
                    }
                }
            }
            DispatchMessage(ref m_removeProtoList);
        }
    }

    //抛出事件
    private static void DispatchMessage(ref List<SProtocol> protoList)
    {
        try
        {
            foreach (SProtocol proto in protoList)
            {
                if (proto != null)
                {
                    byte frameHead = proto.PopName();
                    if (m_dispatchFunctions.ContainsKey(frameHead))
                        m_dispatchFunctions[proto.PopName()](proto);
                }
            }
            protoList.Clear();
        }
        catch (Exception ex)
        {
            Debug.LogError(ex);
        }
    }
    //注册接受调用的函数
    public static void Regist(byte frameHeader, TcpDispatchEvent functionEvent)
    {
        m_dispatchFunctions[frameHeader] = functionEvent;
    }
    //void OnGUI()
    //{
    //    if (GUILayout.Button("发送消息"))
    //    {
    //        string text = Console.ReadLine();
    //        m_server.SendAll(text);
    //    }
    //    if (GUILayout.Button("退出服务器"))
    //    {
    //        if (m_server != null)
    //            m_server.Stop();
    //    }
    //    if (GUILayout.Button("测试"))
    //    {
    //        string message = "#23YA180.55PI-90.55RO050.55AX000.00AY000.00AZ000.00DE100.00TE55HU33E1923084N2606453TW201268";
    //        //将字符串转化为字符数组
    //        Byte[] bytes = System.Text.Encoding.Default.GetBytes(
    //        message.ToCharArray());
    //        //FProtocol proto = new FProtocol();
    //        //proto.Bind(bytes, bytes.Length);
    //        //Debug.Log(proto.PopName());

    //        string log = "";
    //        foreach (byte b in bytes)
    //        {
    //            log += b.ToString();
    //            log += "     ";
    //        }
    //        Debug.Log(log);

    //        int len = BitConverter.ToInt16(bytes, 1);
    //        Debug.Log(len);
    //    }
    //    if (GUILayout.Button("测试2"))
    //    {
    //        short len = 23;
    //        byte[] temp = BitConverter.GetBytes(len);
    //   //     int res = BitConverter.ToInt16(temp,0);
    //        Debug.Log(temp[0] + "  "  + temp[1]);
    //   //     byte[] arr =  BitConverter
    //    }
    //}
    static void Init()
    {
        //   LogFactory.Assign(new ConsoleLogFactory());
        m_server = new AsyncTcpServer(ListenPort);
        m_server.Encoding = Encoding.UTF8;
        m_server.ClientConnected +=
          new EventHandler<TcpClientConnectedEventArgs>(server_ClientConnected);
        m_server.ClientDisconnected +=
          new EventHandler<TcpClientDisconnectedEventArgs>(server_ClientDisconnected);
        //m_server.PlaintextReceived +=
        //  new EventHandler<TcpDatagramReceivedEventArgs<string>>(server_PlaintextReceived);
        m_server.DatagramReceived +=
           new EventHandler<TcpDatagramReceivedEventArgs<byte[]>>(server_DatagramReceived);
        m_server.Start();
        m_protoQueue = new Queue<SProtocol>();
        Debug.Log("TCP server has been started.");
    //    Debug.Log("Type something to send to client...");
    }

    static void server_ClientConnected(object sender, TcpClientConnectedEventArgs e)
    {
        flag = true;
        Debug.Log(string.Format("TCP client {0} has connected.",
          e.TcpClient.Client.RemoteEndPoint.ToString()));
    }

    static void server_ClientDisconnected(object sender, TcpClientDisconnectedEventArgs e)
    {
         flag = false;
      Debug.Log(string.Format( "TCP client {0} has disconnected.",
          e.TcpClient.Client.RemoteEndPoint.ToString()));
    }

    static void server_PlaintextReceived(object sender, TcpDatagramReceivedEventArgs<string> e)
    {
        //Debug.Log(e.Datagram);
        //Debug.Log(e.Datagram.Length);
        //if (e.Datagram != "Received")
        //{
        //    Debug.Log(string.Format("Client : {0} --> ",
        //      e.TcpClient.Client.RemoteEndPoint.ToString()));
        //    Debug.Log(string.Format("{0}", e.Datagram));
        //    m_server.Send(e.TcpClient, "Server has received you text : " + e.Datagram);
        //}
        //SProtocol proto = new SProtocol();
        //int length = Convert.ToInt32(e.Datagram.Substring(1, 2),16);
        //string tmp = e.Datagram.Substring(0, length);
        ////proto.PushName(e.Datagram[0]);
        //proto.Bind(tmp, length);
        //lock (m_protoQueue)
        //{
        //    m_protoQueue.Enqueue(proto);
        //}
    }

    //服务端发送给客户端
    public static void server_DatagramSend(string sender)
    {
        if (!File.Exists(se))
        {
            FileStream fs1 = new FileStream(se, FileMode.Create, FileAccess.Write);//创建写入文件
            StreamWriter sw = new StreamWriter(fs1);
            byte[] b = Encoding.UTF8.GetBytes(sender);//按照指定编码将string编程字节数组
            string result = string.Empty;
            for (int i1 = 0; i1 < b.Length; i1++)//逐字节变为16进制字符
            {
                result += "0x" + Convert.ToString(b[i1], 16) + " ";
            }
            string w = tim + "'" + result + "'" + "\r\n";
            //string w = tim + "'" + sender + "'" + "\r\n";
            sw.Write(w);
            sw.Close();
            fs1.Close();
        }
        else if (File.Exists(se))
        {
            StreamWriter sw = File.AppendText(se);
            byte[] b = Encoding.UTF8.GetBytes(sender);//按照指定编码将string编程字节数组
            string result = string.Empty;
            for (int i1 = 0; i1 < b.Length; i1++)//逐字节变为16进制字符
            {
                result += "0x" + Convert.ToString(b[i1], 16) + " ";
            }
            string w = tim + "'" + result + "'" + "\r\n";
            //string w = tim + "'" + sender + "'" + "\r\n";
            sw.Write(w);
            sw.Close();
        }
        //m_server = new AsyncTcpServer(ListenPort);
        Byte[] bytes = System.Text.Encoding.UTF8.GetBytes(
               sender.ToCharArray());
        m_server.SendAll(sender);
    }

    static void server_DatagramReceived(object sender, TcpDatagramReceivedEventArgs<byte[]> e)
    {
        string t = System.Text.Encoding.UTF8.GetString(e.Datagram);
        if (!File.Exists(re))
        {
            FileStream fs1 = new FileStream(re, FileMode.Create, FileAccess.Write);//创建写入文件
            StreamWriter sw = new StreamWriter(fs1);
            byte[] b = Encoding.UTF8.GetBytes(t);//按照指定编码将string编程字节数组
            string result = string.Empty;
            for (int i1 = 0; i1 < b.Length; i1++)//逐字节变为16进制字符
            {
                result += "0x" + Convert.ToString(b[i1], 16) + " ";
            }
            string w = tim + "'" + result + "'" + "\r\n";
            //string w = tim + "'" + t + "'" + "\r\n";
            sw.Write(w);
            sw.Close();
            fs1.Close();
        }
        else if (File.Exists(re))
        {
            StreamWriter sw = File.AppendText(re);
            byte[] b = Encoding.UTF8.GetBytes(t);//按照指定编码将string编程字节数组
            string result = string.Empty;
            for (int i1 = 0; i1 < b.Length; i1++)//逐字节变为16进制字符
            {
                result += "0x" + Convert.ToString(b[i1], 16) + " ";
            }
            string w = tim + "'" + result + "'" + "\r\n";
            //string w = tim + "'" + t + "'" + "\r\n";
            sw.Write(w);
            sw.Close();
        }
        SProtocol proto = new SProtocol();
        int i = 0;
        foreach (char a in t)
        {
            if (a == '#')
            {
                int length = Convert.ToInt32(t.Substring(i + 1, 2), 16);
                string tmp = t.Substring(i, length);
                //proto.PushName(e.Datagram[0]);
                proto.Bind(tmp, length);
                lock (m_protoQueue)
                {
                    m_protoQueue.Enqueue(proto);
                }
            }
            else if (a == '*' || a == '~')
            {
                int length = Convert.ToInt32(t.Substring(i + 1, 1), 16);
                string tmp = t.Substring(i, length);
                //proto.PushName(e.Datagram[0]);
                proto.Bind(tmp, length);
                lock (m_protoQueue)
                {
                    m_protoQueue.Enqueue(proto);
                }
            }
            i++;
        }
    }
}
