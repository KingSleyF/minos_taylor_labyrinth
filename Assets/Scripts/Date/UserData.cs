﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UserData : MonoBehaviour {

    public AudioClip Ac_Begin;
    public AudioSource As_Begin;

    static public string[] UserName;
    static public string[] UserPwd;
    static public int Usernumber = 0;
    Text input_name;
    Text input_pwd;
    public GameObject TalkBack;

    bool HavingSave = false;//账号信息是否已经存在

	// Use this for initialization
	void Start ()
    {
        UserName = new string[100];
        UserPwd = new string[100];
        input_name = GameObject.Find("input_name").GetComponent<Text>();
        input_pwd = GameObject.Find("input_pwd").GetComponent<Text>();
        Usernumber = PlayerPrefs.GetInt("UserNumber", 0);
        for (int j = 0; j < Usernumber; j++)
        {
            UserName[j] = PlayerPrefs.GetString("Name" + j, "NULL");
            UserPwd[j] = PlayerPrefs.GetString("Pwd" + j, "NULL");
        }
        if (Usernumber == 0) 
            HavingSave = false;
        else
        {
            Debug.Log("账号：" + UserName[0] + "   密码：" + UserPwd[0]);
            HavingSave = true;
            //input_name.text = UserName[0];
            //input_pwd.text = UserPwd[0];
        }
    }
	
	// Update is called once per frame
	void Update ()
    {

    }

    //未注册提示
    void WeiZhuCe()
    {
        TalkBack.SetActive(true);
        GameObject.Find("Talk").GetComponent<Text>().text = "用户尚未注册！";
    }

    //账号密码错误提示
    void ZhangHaoCuoWu()
    {
        TalkBack.SetActive(true);
        GameObject.Find("Talk").GetComponent<Text>().text = "帐号密码错误！";
    }

    //注册信息错误
    void ZhuCeCuoWu()
    {
        TalkBack.SetActive(true);
        GameObject.Find("Talk").GetComponent<Text>().text = "注册信息错误！";
    }

    //注册成功
    void ZhuCeChengGong()
    {
        TalkBack.SetActive(true);
        GameObject.Find("Talk").GetComponent<Text>().text = "注册成功，账号：" + UserName[Usernumber] + "   密码：" + UserPwd[Usernumber];
    }

    //自动消失
    void ImageEnd()
    {
        TalkBack.SetActive(false);
    }

    //注册按钮响应函数
    public void Click_Zhu()
    {
        if (input_name.text != "NULL" && input_pwd.text != "NULL" && input_name.text != "" && input_pwd.text != "")
        {
            UserName[Usernumber] = input_name.text;
            UserPwd[Usernumber] = input_pwd.text;
            Debug.Log("注册成功，账号：" + UserName[Usernumber] + "   密码：" + UserPwd[Usernumber]);
            ZhuCeChengGong();
            Invoke("ImageEnd", 2.0f);
            PlayerPrefs.SetString("Name" + (Usernumber), UserName[Usernumber]);
            PlayerPrefs.SetString("Pwd" + (Usernumber), UserPwd[Usernumber]);
            Usernumber++;
            PlayerPrefs.SetInt("UserNumber", Usernumber);
            HavingSave = true;
        }
        else
        {
            Debug.Log("注册信息错误！");
            ZhuCeCuoWu();
            Invoke("ImageEnd", 2.0f);
        }
    }

    //开始按钮响应函数
    public void Click_Shi()
    {
        if (HavingSave) 
        {
            int j;
            for (j = 0; j < Usernumber; j++) 
            {
                if (input_name.text == UserName[j] && input_pwd.text == UserPwd[j]) 
                {
                    Debug.Log("进入游戏加载");
                    As_Begin.clip = this.Ac_Begin;
                    As_Begin.loop = true;
                    As_Begin.volume = 1f;
                    As_Begin.Play();
                    StartCoroutine(GameBeg(2.0f));
                }
                if (j == Usernumber - 1)
                {
                    Debug.Log("帐号密码错误！");
                    ZhangHaoCuoWu();
                    Invoke("ImageEnd", 2.0f);
                }
            }
        }
        else
        {
            Debug.Log("用户尚未注册！");
            WeiZhuCe();
            Invoke("ImageEnd", 2.0f);
        }
    }

    IEnumerator GameBeg(float time)
    {
        yield return new WaitForSeconds(time);
        SceneManager.LoadScene("Loading");
    }

    public void TuiChu()
    {
        Application.Quit();
    }
}
